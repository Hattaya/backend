const express = require('express')
const router = express.Router()
const userController = require('../controller/StatusController')

router.get('/', function (req, res, next) {
  res.json(userController.getState())
})

module.exports = router

const User = require('../models/Professor')
const professorController = {
  professorList: [
    {
      id: 1,
      name: 'ชัยชนะ สุดใจรัก',
      branch: 'CS',
      phoneNumber: '0861471058',
      email: 'chaichana@gmail.com'
    },
    {
      id: 2,
      name: 'เจิมขวัญ อยู่ยิ่ง',
      branch: 'CS',
      phoneNumber: '0812345678',
      email: 'chermkwan@gmail.com'
    }
  ],
  lastId: 3,
  async addProfessor (req, res, next) {
    const payload = req.body
    // User.create(payload).then(function(user) {
    //   res.json(user)
    // }).catch(function(err){
    //   res.status(500).send(err)
    // })
    const user = new User(payload)
    try {
      await user.save()
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },

  async updateProfessor (req, res, next) {
    const payload = req.body
    try {
      const user = await User.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },

  async deleteProfessor (req, res, next) {
    const { id } = req.params
    try {
      const user = await User.deleteOne({ _id: id })
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },

  async getProfessors (req, res, next) {
    // User.find({}).exec(function (err, users) {
    //   if (err) {
    //     res.status(500).send()
    //   }
    //   res.json(users)
    // })
    // User.find({}).then(function (users) {
    //   res.json(users)
    // }).catch(function (err) {
    //   res.status(500).send(err)
    // })
    // Async Await
    try {
      const users = await User.find({})
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },

  async getProfessor (req, res, next) {
    const { id } = req.params
    try {
      const user = await User.findById(id)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = professorController

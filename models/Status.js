const mongoose = require('mongoose')
const stateSchema = new mongoose.Schema({
  topic: {
    type: String,
    required: true
  },
  status: {
    type: String,
    required: true
  },
  detail: {
    type: String,
    required: true
  }
})

module.exports = mongoose.model('Professor', stateSchema)
